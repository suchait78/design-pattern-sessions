package observer;

public class Client {

    public static void main(String[] args){

        Data data = new Data(12,"suchait");

        CricketServer server = new CricketServer(data);

        Observer observer1 = new Cricbuzz();
        Observer observer2 = new Website2();

        server.addObserver(observer1);
        server.addObserver(observer2);

        server.addRun(data, 4);

        server.addRun(data,4);

    }
}
