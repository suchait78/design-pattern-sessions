package observer;

public class Website2 implements Observer{


    @Override
    public void update(Data data) {
        System.out.println();

        System.out.println("Website2 updated Report");
        System.out.println(data);
    }
}
