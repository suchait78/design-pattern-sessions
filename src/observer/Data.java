package observer;

public class Data {

    private long dataId;
    private String playerName;
    private int runs;

    public Data(long dataId, String playerName) {
        this.dataId = dataId;
        this.playerName = playerName;
    }

    public String getPlayerName() {
        return playerName;
    }

    public void setPlayerName(String playerName) {
        this.playerName = playerName;
    }

    public int getRuns() {
        return runs;
    }

    public void setRuns(int runs) {
        this.runs = runs;
    }

    @Override
    public String toString() {
        return "Data{" +
                "dataId=" + dataId +
                ", playerName='" + playerName + '\'' +
                ", runs='" + runs + '\'' +
                '}';
    }
}
