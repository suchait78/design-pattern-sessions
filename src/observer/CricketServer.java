package observer;

import java.util.ArrayList;
import java.util.List;

public class CricketServer {

    private List<Observer> observerList;
    private Data data;

    public CricketServer(Data data) {
        this.data = data;
        this.observerList = new ArrayList<>();
    }

    public CricketServer(List<Observer> observerList) {
        this.observerList = new ArrayList<>();
    }

    public void addObserver(Observer observer){
        this.observerList.add(observer);
    }

    public void removeObserver(Observer observer){
        this.observerList.remove(observer);
    }

    public void addRun(Data data, int run){

        this.data.setRuns(this.data.getRuns() + run);
        this.observerList.forEach(observer ->observer.update(this.data));
    }

}
