package adapter;

public class CardDesigner {

    Customer customer;

    public CardDesigner(Customer customer) {
        this.customer = customer;
    }

    public void designCard(){

        System.out.println();
        System.out.println("Printing card below");
        System.out.println("Name : " + customer.getName());
        System.out.println("Work : " + customer.getWork());
        System.out.println("Address : " + customer.getAddress());

    }
}
