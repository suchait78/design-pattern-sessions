package adapter;

public class EmployeeAdapter implements Customer{

    Employee employee;

    public EmployeeAdapter(Employee employee) {
        this.employee = employee;
    }

    @Override
    public String getName() {
        return this.employee.getEmpFirstName() + " " + this.employee.getEmpSecondName();
    }

    @Override
    public String getWork() {
        return this.employee.getEmpDept();
    }

    @Override
    public String getAddress() {
        return this.employee.getEmpAddress();
    }
}
