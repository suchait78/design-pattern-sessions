package adapter;

public interface Customer {

    public String getName();
    public String getWork();
    public String getAddress();
}
