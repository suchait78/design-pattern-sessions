package adapter;

public class Employee {

    private String empFirstName;
    private String empSecondName;
    private String empDept;
    private String empAddress;

    public Employee(String empFirstName, String empSecondName, String empDept, String empAddress) {
        this.empFirstName = empFirstName;
        this.empSecondName = empSecondName;
        this.empDept = empDept;
        this.empAddress = empAddress;
    }

    public String getEmpFirstName() {
        return empFirstName;
    }

    public String getEmpSecondName() {
        return empSecondName;
    }

    public String getEmpDept() {
        return empDept;
    }

    public String getEmpAddress() {
        return empAddress;
    }
}
