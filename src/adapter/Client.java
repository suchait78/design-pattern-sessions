package adapter;

public class Client {

    public static void main(String[] args){

        Employee emp =
                new Employee("Suchait", "Gaurav","IT","dd nagar");

        EmployeeAdapter customerAdapter = new EmployeeAdapter(emp);

        CardDesigner designer = new CardDesigner(customerAdapter);
        designer.designCard();



    }
}
